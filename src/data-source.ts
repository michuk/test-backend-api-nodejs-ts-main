import { DataSource } from "typeorm";
import { Ship } from "./entity/ShipEntity";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "test",
  password: "test",
  database: "test",
  synchronize: true,
  logging: false,
  entities: [Ship],
  subscribers: [],
  migrations: [],
});

(async () => {
  try {
    await AppDataSource.initialize();
  } catch (error) {
    console.error(error);
  }
})();
