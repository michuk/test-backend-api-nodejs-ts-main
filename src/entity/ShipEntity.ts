import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Ship {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "varchar" })
  name: string;

  @Column({ type: "varchar" })
  speed: string;
}
