import express from "express";
import "reflect-metadata";
import { AppDataSource } from "./data-source";
import * as bodyParser from "body-parser";
import { Ship } from "./entity/ShipEntity";
const app = express();
const port = 3000;
app.use(bodyParser.urlencoded({ extended: false }));
app.get("/ships", async (req, res) => {
  const ships = await AppDataSource.getRepository(Ship).find();
  res.status(200);
  res.json(ships);
});

app.post("/ships", async (req, res) => {
  if (!req.body) {
    res.sendStatus(400);
    return;
  }
  const { name, speed } = req.body;
  if (!name || !speed) {
    res.sendStatus(400);
    return;
  }
  try {
    const repo = AppDataSource.getRepository(Ship);
    const ship = repo.create({ name, speed });
    await repo.save(ship);
    res.status(201).json(ship);
  } catch (exc) {
    res.status(500).send(exc);
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
