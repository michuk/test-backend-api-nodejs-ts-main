# Task: Backend API in Node.js and TypeScript

Implement API server in Node.js and TypeScript based on the API specification. You can choose any dev-stack you like.

It shouldn't take more than one hour to complete it.

We like:
- Tests 
- Nice commit messages
- Working code
- README

Aditional notes:
- Best way how to share the code with us is to create new GitHub repo.
- You don't have to finish everything. If you don't, just put there a comment describing your intent and we can then discuss it later together.
